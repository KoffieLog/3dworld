#pragma once
int main();

void handle_input();

void render_scene_to_buffer(
	uint8_t* pBuffer,
	std::vector<Object*> scene_onjects);

void set_projeciton_matrix(
	Eigen::Matrix4f& mat,
	float clip_n,
	float clip_f,
	int h,
	int w,
	float fov);


Eigen::Matrix4f AngleAxisToMatrix(
	float angle_deg, 
	Eigen::Vector3f axis);

