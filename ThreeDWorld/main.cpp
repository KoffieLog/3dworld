#include "pch.h"
#include "main.h"
#include "Bresenham.h"
#include "RigidBody.h"


// Window and camera properties
int width, height;        
float fov, clip_near, clip_far;
Eigen::Matrix4f camera_t, camera_R, camera_proj;

// camera physics
std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
RigidBody camera_physics;
Eigen::Vector3f v1(0, 0, 0);


float dt = 0;
float speed = 1;		// meters per second
float jump_speed = 2;  // meters per second
bool is_jump = false;   // spaecbar --> jump

std::vector<Object*> scene_objects;
uint8_t* buffer;


int main() {

	// initialize camera properties
	camera_t = camera_R = Eigen::Matrix4f::Identity();
	camera_proj = Eigen::Matrix4f::Zero();             
	width = 800, height = 600;
	clip_near = 0.01, clip_far = 10;
	fov = 110;
	set_projeciton_matrix(camera_proj, clip_near, clip_far, height, width, fov);

	// init camera physics
	camera_physics.SetMass(75);  // kg
	camera_physics.SetDrag(0.5); // friction

	// make some 3d object
	Cube the_cube;
	the_cube.SetPosition(0, 0, -1);
	the_cube.SetScale(.2, .2, .2);
	scene_objects.push_back(dynamic_cast<Object*>(&the_cube));

	// initialize display window pixels
	buffer = new unsigned char[width * height];
	memset(buffer, 0x0, width * height);

	// use opencv to create a window, and point image date to buffer
	const char* window_name = "3D World";
	cv::Mat cv_render = cv::Mat(height, width, CV_8UC1, (void*)buffer);
	cv::namedWindow(window_name);

	while (cv::getWindowProperty(window_name, cv::WND_PROP_VISIBLE) >= 1) {

		// advance clock
		auto t0 = std::chrono::high_resolution_clock::now();
		dt = std::chrono::duration<float>(t0 - t1).count();
		t1 = t0;

		handle_input();

		// rotate the cube at 60 degrees second second
		float _omega = 60;
		auto R = AngleAxisToMatrix(_omega * dt, { 0, 1, 0 });
		the_cube.local_to_world *= R;

		// render to image buffer
		render_scene_to_buffer(buffer, scene_objects);

		// update output image
		cv::imshow(window_name, cv_render);
		cv::waitKey(10);
	}

	delete[] buffer;
	return 0;
}

Eigen::Matrix4f AngleAxisToMatrix(
	float angle_deg, 
	Eigen::Vector3f axis) {

	Eigen::Matrix4f R = Eigen::Matrix4f::Identity();
	R.block<3, 3>(0, 0) = Eigen::AngleAxis<float>(angle_deg * M_PI / 180, axis).matrix();
	return R;
}

void handle_input() {

	Eigen::Vector3f a_t(0, -9.81, 0);
	Eigen::Vector3f v_t(0, 0, 0);

	if (GetAsyncKeyState('W') & 0x8000) v_t.z() -= speed;
	if (GetAsyncKeyState('A') & 0x8000) v_t.x() -= speed;
	if (GetAsyncKeyState('S') & 0x8000) v_t.z() += speed;
	if (GetAsyncKeyState('D') & 0x8000) v_t.x() += speed;
	if (GetAsyncKeyState(VK_SPACE) & 0x8000) if (!is_jump) {
		v_t.y() += jump_speed; 
		is_jump = true; }
	if (GetAsyncKeyState(VK_ESCAPE) & 0x8000) {
	}

	v1 += a_t * dt;
	v_t.y() += v1.y();

	// calc new position
	Eigen::Vector3f pos_old = camera_t.block<3, 1>(0, 3);
	Eigen::Vector3f pos_new = pos_old + v_t * dt;

	// prevent from falling through floor
	if (pos_new.y() < 0) {
		pos_new.y() = 0;
		is_jump = false;
	}

	// update camera
	camera_t.block<3, 1>(0, 3) = pos_new;
	v1 = (pos_new - pos_old) / dt;
}

void set_projeciton_matrix(
	Eigen::Matrix4f& mat,
	float n,
	float f,
	int h,
	int w,
	float fov) {

	float a = w / (float)h;

	float scale = tan(fov * 0.5 * M_PI / 180) * n;
	float r = a * scale;
	float l = -r;
	float t = scale;
	float b = -t;

	// col 0
	mat(0, 0) = 2 * n / (r - l);
	mat(1, 0) = 0;
	mat(2, 0) = 0;
	mat(3, 0) = 0;

	// col 1
	mat(0, 1) = 0;
	mat(1, 1) = 2 * n / (t - b);
	mat(2, 1) = 0;
	mat(3, 1) = 0;

	// col 2
	mat(0, 2) = (r + l) / (r - l);
	mat(1, 2) = (t + b) / (t - b);
	mat(2, 2) = -(f + n) / (f - n);
	mat(3, 2) = -1;

	// col 3
	mat(0, 3) = 0;
	mat(1, 3) = 0;
	mat(2, 3) = -2 * f * n / (f - n);
	mat(3, 3) = 0;
}

void render_scene_to_buffer(
	uint8_t* pBuffer,
	std::vector<Object*> scene_onjects) {

	// clear image to black
	memset(pBuffer, 0x0, width * height);

	// update camera world projection transform
	Eigen::Matrix4f M = camera_proj * (camera_t * camera_R).inverse();

	for (const auto obj : scene_onjects) {

		// object vertices to perspective space
		auto verts_proj = 
			M * obj->local_to_world * obj->mesh->vertices().colwise().homogeneous();

		// draw triangles from verts
		Eigen::Matrix<int, 3, 12> triangles = obj->mesh->triangles();
		for (int i = 0; i < triangles.cols(); i++) {

			// draw 3 lines from tringle corners
			auto tri = triangles.col(i);
			for (auto j = 0; j < 3; j++)
			{
				// w normaized projeciton of vertex at both end of triangle line
				Eigen::Vector4f p0 = verts_proj.col(tri[j]);
				Eigen::Vector4f p1 = verts_proj.col(tri[(j + 1) % 3]);
				p0.block<3, 1>(0, 0) /= p0.w();
				p1.block<3, 1>(0, 0) /= p1.w();

				// apply primitive clipping
				if (abs(p0.x()) > 1 || abs(p1.x()) > 1 || 
					abs(p0.y()) > 1 || abs(p1.y()) > 1 ||
					abs(p0.z()) > 1 || abs(p1.z()) > 1) {
					continue;
				}

				// map to pixel coords
				int u0 = std::min((uint32_t)width - 1, (uint32_t)((p0.x() + 1) * 0.5 * width));
				int v0 = std::min((uint32_t)height - 1, (uint32_t)((1 - (p0.y() + 1) * 0.5) * height));
				int u1 = std::min((uint32_t)width - 1, (uint32_t)((p1.x() + 1) * 0.5 * width));
				int v1 = std::min((uint32_t)height - 1, (uint32_t)((1 - (p1.y() + 1) * 0.5) * height));

				//get pixels from p1 to p2
				std::vector<std::tuple<int, int>> line;
				Bresenham(u0, v0, u1, v1, line);

				// draw pixels
				for (auto& uv : line) {
					std::ptrdiff_t offset = std::get<1>(uv) * width + std::get<0>(uv);
					*(pBuffer + offset) = 255;
				}		
			}
		}
	}

	//std::ofstream ofs;
	//ofs.open("./out.pgm");
	//ofs << "P5\n" << width << " " << height << "\n255\n";
	//ofs.write((char*)pBuffer, width * height);
	//ofs.close();
}
