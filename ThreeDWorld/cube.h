#pragma once
#include "object.h"

class Cube : public Object
{
public:

	Cube() {
		this->mesh = new Mesh(m_verts, nullptr, m_tris, 8, 12);
	}

private:
	const float m_verts[3 * 8] {
		-0.5, +0.5, -0.5,
		+0.5, +0.5, -0.5,
		-0.5, -0.5, -0.5, 
		+0.5, -0.5, -0.5,
		-0.5, +0.5, +0.5,
		+0.5, +0.5, +0.5,
		-0.5, -0.5, +0.5,
		+0.5, -0.5, +0.5,
	};

	const int m_tris[36] {
		0, 1, 2,    // side 1
		2, 1, 3,
		4, 0, 6,    // side 2
		6, 0, 2,
		7, 5, 6,    // side 3
		6, 5, 4,
		3, 1, 7,    // side 4
		7, 1, 5,
		4, 5, 0,    // side 5
		0, 5, 1,
		3, 7, 2,    // side 6
		2, 7, 6,
	};
};

