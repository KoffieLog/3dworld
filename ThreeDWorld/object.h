#pragma once
#include "mesh.h"

class Object
{
public:

	Eigen::Matrix4f local_to_world = Eigen::Matrix4f::Identity();

	void SetPosition(float x, float y, float z) {
		this->local_to_world.block<3,1>(0,3) << x, y, z;
	}

	Eigen::Vector3f GetPosition() {
		return this->local_to_world.block<3, 1>(0, 3);
	}

	void SetScale(float sx, float sy, float sz) {
		this->local_to_world.block<3, 3>(0, 0).colwise().normalize();
		this->local_to_world.block<3, 1>(0, 0) *= sx;
		this->local_to_world.block<3, 1>(0, 1) *= sy;
		this->local_to_world.block<3, 1>(0, 2) *= sz;
	}

	Mesh* mesh = nullptr;
};

