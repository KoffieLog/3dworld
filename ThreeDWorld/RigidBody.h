#pragma once
#include "Eigen/dense"

class RigidBody
{

	public:

		RigidBody() {

			this->use_gravity = true;
			this->mass = 75;
			this->drag = 0.5;

			Reset();
		}

		void Reset() {
			this->position << 0, 0, 0;
			this->velocity << 0, 0, 0;
			this->accellaration << 0, 0, 0;
			this->external_force << 0, 0, 0;
			this->impulse << 0, 0, 0;
		}

		void UseGravity(bool is_true) { this->use_gravity = is_true; }
		void SetMass(float value) { this->mass = value; }
		void SetDrag(float value) { this->drag = value; }
		void SetPosition(float x, float y, float z) { this->position << x, y, z; }
		void SetVelocity(float dx, float dy, float dz) { this->velocity << dx, dy, dz; }
		void AddForce(float fx, float fy, float fz) { this->external_force += Eigen::Vector3f(fx, fy, fz); }
		void AddImpulse(float fx, float fy, float fz) { this->impulse += Eigen::Vector3f(fx, fy, fz); }
		Eigen::Vector3f GetPosition() { return this->position; }

		void Update(float dt) {



		}

	private:
		bool use_gravity = true;

		float mass;           // kg
		float drag;           // dimensionless

		Eigen::Vector3f position;		// xyz [m]
		Eigen::Vector3f velocity;		// d_xyz [m/s]
		Eigen::Vector3f accellaration;	// dd_xyz [m/s*s]
		Eigen::Vector3f external_force; // kg * dd_xyz [kg * m / s*s]
		Eigen::Vector3f impulse;
};


