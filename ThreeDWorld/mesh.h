#pragma once
struct Mesh {

	typedef Eigen::Matrix<float, 3, Eigen::Dynamic> mesh_t;
	typedef Eigen::Matrix<int, 3, Eigen::Dynamic> tris_t;


public:

	Mesh(const float* vert_data,
		const float* norm_data,
		const int* tris_data,
		const std::size_t nVerts,
		const std::size_t nTris) :
		m_verts(vert_data),
		m_norms(norm_data),
		m_tris(tris_data),
		m_nVerts(nVerts),
		m_nTris(nTris) {	}

	Eigen::Map<const mesh_t> vertices() {
		return Eigen::Map<const mesh_t>(
			this->m_verts, 3, this->m_nVerts);
	}

	Eigen::Map<const tris_t> triangles() {
		return Eigen::Map<const tris_t>(
			this->m_tris, 3, this->m_nTris);
	}

	const int m_nTris = 0;
	const int m_nVerts = 0;
	const float* m_verts = nullptr;
	const float* m_norms = nullptr;
	const int* m_tris = nullptr;
};
